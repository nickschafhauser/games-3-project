// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _ENEMYSHIP_H                 // Prevent multiple definitions if this 
#define _ENEMYSHIP_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"
#include <vector>
#include "SchafhauserEnemyBullet.h"
#include <time.h>

namespace EnemyNS
{
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                  // image height
	const int   TEXTURE_COLS = 4;           // texture has 8 columns
	//BARON actions
	const int ENEMY_START = 24;				
	const int ENEMY_END = 24;
	
	const int ENEMY_IDLE_START = 24;				
	const int ENEMY_IDLE_END = 24;
	const float SPEED = 200.0f;                // 100 pixels per second
    const float MASS = 100.0f;         // image height
	const float ROTATION_RATE = (float)PI;

	const int   ENEMY_DESTROYED_START = 8;    // damage start frame
	const int   ENEMY_DESTROYED_END = 23;      // damage end frame

	const float ENEMY_ANIMATION_DELAY = 0.08f;    // time between frames
	const float ENEMY_IMAGE_SCALE = 1.1f;
}

// inherits from Entity class
class EnemyShip : public Entity
{
private:
	float timeInState;
	float randomCooldown;

	bool enemyShipDead;
	Image enemyDestroyed;
public:
    // constructor
    EnemyShip();

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    //void damage(WEAPON);
	void fire(void);
	std::vector<EnemyBullet> bulletVec;
	std::vector<EnemyBullet> bulletsOnScreen;
	bool isDead;
	void setDead(bool d){enemyShipDead = d;};
	void recycleBullet(int index);

	void setInvisible() {Image::setVisible(false); Entity::setActive(false); isDead = true;} //makes the enemy ship invisible

	void reset(float x, float y);
};
#endif

