// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "KenneyAIShip.h"

//=============================================================================
// default constructor
//=============================================================================
AIShip::AIShip() : Entity()
{
    spriteData.width = AINS::WIDTH;           // size of Ship1
    spriteData.height = AINS::HEIGHT;
    spriteData.rect.bottom = AINS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = AINS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = AINS::AI_ANIMATION_DELAY;
    startFrame = AINS::AI_IDLE_START;     // first frame of ship animation
	endFrame     = AINS::AI_IDLE_END;     // last frame of ship animation
	currentFrame = startFrame;
	radius = 17;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	//TODO: use this for damage
	//dead = false;
	mass = AINS::MASS;
	collisionType = entityNS::CIRCLE;
	health = 5;
	speed = 300;
	AIshipDead = false;
	timeInState = 0.0;
	srand (time(NULL));
	randomCooldown = 1.5; //between 3 and 16 seconds
	health = 3;
	hit = false;
	doneFiring = false;
	sent = false;
	//deadAI = false;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool AIShip::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    AIHit.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    AIHit.setFrames(AINS::AI_HIT_START, AINS::AI_HIT_END);
    AIHit.setCurrentFrame(AINS::AI_HIT_START);
    AIHit.setFrameDelay(AINS::AI_ANIMATION_DELAY);
	AIHit.setLoop(false);

	AIDestroyed.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    AIDestroyed.setFrames(AINS::AI_DESTROYED_START, AINS::AI_DESTROYED_END);
    AIDestroyed.setCurrentFrame(AINS::AI_DESTROYED_START);
    AIDestroyed.setFrameDelay(AINS::AI_ANIMATION_DELAY);
	AIDestroyed.setLoop(false);

    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void AIShip::draw()
{
    Image::draw();              // draw ship
	//TODO: use this for damage
	//TODO: use this for damage
	if (!bulletsOnScreen.empty()){
		for (int i = 0; i < bulletsOnScreen.size(); i++){
			bulletsOnScreen[i].draw();
			//bulletsOnScreen.pop_back(); //take the bullet out of the array
		}
	}
	if ((hit)&&(!AIshipDead)){
		AIHit.draw(spriteData, 0);
	}
	if (AIshipDead){
		AIDestroyed.draw(spriteData, 0);
		doneFiring = true;
	}

}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void AIShip::update(float frameTime)
{
	//Entity::update(frameTime);
	spriteData.angle = AINS::ROTATION_RATE;  // rotate the ship

	VECTOR2 foo = velocity*frameTime*speed;
	if (getPositionX() + Image::getWidth()*Image::getScale() > GAME_WIDTH)
	{
		setPosition(D3DXVECTOR2(0,getPositionY()));
	}
	if (getPositionX() < 0)
	{
		setPosition(D3DXVECTOR2(GAME_WIDTH-Image::getWidth()*Image::getScale(),getPositionY()));
	}
	//if (getPositionY() + Image::getHeight()*Image::getScale() > GAME_HEIGHT)
	//{
	//	setPosition(D3DXVECTOR2(getPositionX(),0));
	//}
	//if (getPositionY() < 0)
	//{
	//	setPosition(D3DXVECTOR2(getPositionX(),GAME_WIDTH-Image::getHeight()*Image::getScale()));
	//}
	timeInState += frameTime;
	if ((timeInState > randomCooldown) && (!doneFiring)){ //once frametime is >= our random time, it will fire. then a new random between 3 and 16 seconds will be calculated
		timeInState = 0; //reset the timer
		//randomCooldown = rand()%12 + 3;
		fire();
	}
	if (!bulletsOnScreen.empty()){
		for (int i = 0; i < bulletsOnScreen.size(); i++){
			bulletsOnScreen[i].update(frameTime);

			if (bulletsOnScreen[i].getY() >= GAME_HEIGHT + 100){ //off the bot of the screen
				//put the bullet back into the bulletVec vector
				/*bulletVec.push_back(bulletsOnScreen[i]);
				bulletsOnScreen.erase(bulletsOnScreen.begin() + i); */
				recycleBullet(i);

				/*bulletVec.back().setVisible(true); 
				bulletVec.back().setActive(true);*/
				//bulletVec.back().setVisible(true);
			}
		}
	}

	//taking damage
	if(hit)
    {
        AIHit.update(frameTime);
        if(AIHit.getAnimationComplete())
        {
            hit = false;
			AIHit.setAnimationComplete(false);
			AIHit.setCurrentFrame(AINS::AI_HIT_START);
		}
	}

	if(AIshipDead)
	{
		AIDestroyed.update(frameTime);
		if(AIDestroyed.getAnimationComplete())
		{
			AIshipDead = false;
			AIDestroyed.setAnimationComplete(true);
			AIDestroyed.setCurrentFrame(AINS::AI_DESTROYED_START);
		}
	}

	velocity = D3DXVECTOR2(0,0);
	incPosition(foo);
	Image::setX(getPositionX());
	//Image::setY(getPositionY());
    Entity::update(frameTime);
	//dying
	
}

void AIShip::sendAIShip(){
	velocity.y = 100;
	velocity.x = 20;
}


void AIShip::vectorTrack(){
	
	VECTOR2 vel = targetEntity.getCenterPoint() - this->getCenterPoint();
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(*foo);
}

void AIShip::blindShooting(float frameTime){
	
	VECTOR2 vel = targetEntity.getCenterPoint() - this->getCenterPoint();
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(*foo);
	timeInState += frameTime;
	if (timeInState > .3){ //once frametime is >= our random time, it will fire. then a new random between 3 and 16 seconds will be calculated
		timeInState = 0; //reset the timer
		//randomCooldown = rand()%12 + 3;
		fire();
	}

}

void AIShip::ai(float time, Entity &t){
	targetEntity = t;
	//vectorTrack();
	return;
}

void AIShip::fire(){
	if (!bulletVec.empty()){
	bulletsOnScreen.push_back(bulletVec.back());
	//bulletsOnScreen.back().setVisible(true);
	bulletsOnScreen.back().setVisible(true); 
	bulletsOnScreen.back().setActive(true);

	bulletsOnScreen.back().setX(spriteData.x+12.5); //sets the bullets x coordinate to whereever the EnemyShip was when it fired
	bulletsOnScreen.back().setY(spriteData.y + 50); //sets the bullets y coordinate to whereever the EnemyShip was when it fired
	bulletVec.pop_back();
	
	audio->playCue(AIFIRE);
	}
}

void AIShip::recycleBullet(int index){
	bulletVec.push_back(bulletsOnScreen[index]);
	bulletsOnScreen.erase(bulletsOnScreen.begin() + index); 
}

void AIShip::reset(float x, float y){
	isDead = false;
	this->health = 3;
	this->hit = false;
	this->doneFiring = false;
	this->AIshipDead = false;
	this->setVisible(true); 
	this->setActive(true);
	this->setX(0);
	this->setY(0);
	sent = false;
}