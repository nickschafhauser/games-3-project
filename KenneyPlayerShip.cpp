// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "KenneyPlayerShip.h"
#include "SmokeparticleManager.h"


//=============================================================================
// default constructor
//=============================================================================
PlayerShip::PlayerShip() : Entity()
{
    spriteData.width = PlayerNS::WIDTH;           // size of Ship1
    spriteData.height = PlayerNS::HEIGHT;
    spriteData.rect.bottom = PlayerNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = PlayerNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = PlayerNS::PLAYER_ANIMATION_DELAY;
    startFrame = PlayerNS::PLAYER_IDLE_START;     // first frame of ship animation
	endFrame     = PlayerNS::PLAYER_IDLE_END;     // last frame of ship animation
	currentFrame = startFrame;
	radius = 17;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	mass = PlayerNS::MASS;
	collisionType =   entityNS::CIRCLE;
	health = 5;

	playerDead = false;
	dead = false;
	hit = false;
	timeInState = 1.0;
	lives = 3;
	isDead = false;
	score = 0;
	numberOfEnemiesKilled = 0;

	level = true;
	shooting = false;
	level2 = false;
	shaker = left;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool PlayerShip::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
	//TODO: initialize the image that is being used for damage
    playerHit.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    playerHit.setFrames(PlayerNS::PLAYER_HIT_START, PlayerNS::PLAYER_HIT_END);
    playerHit.setCurrentFrame(PlayerNS::PLAYER_HIT_START);
    playerHit.setFrameDelay(PlayerNS::PLAYER_ANIMATION_DELAY);
	playerHit.setLoop(false);

	playerDestroyed.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    playerDestroyed.setFrames(PlayerNS::PLAYER_DESTROYED_START, PlayerNS::PLAYER_DESTROYED_END);
    playerDestroyed.setCurrentFrame(PlayerNS::PLAYER_DESTROYED_START);
    playerDestroyed.setFrameDelay(PlayerNS::PLAYER_ANIMATION_DELAY);
	playerDestroyed.setLoop(false);

    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}


//=============================================================================
// draw the ship
//=============================================================================
void PlayerShip::draw()
{
	Image::draw();              // draw ship
	//TODO: use this for damage

	if (!bulletsOnScreen.empty()){
		for (int i = 0; i < bulletsOnScreen.size(); i++){
			bulletsOnScreen[i].draw();

			//bulletsOnScreen.pop_back(); //take the bullet out of the array
		}
	}
	if ((hit)&&(!dead)){
		playerHit.draw(spriteData, 0);
	}
	if (dead)
		playerDestroyed.draw(spriteData, 0);
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void PlayerShip::update(float frameTime)
{
	
	Entity::update(frameTime);
	
	//=======================================================================
	//								MOVEMENT
	//=======================================================================
	int directionX = 0;
	int directionY = 0;
	
	//if (!getHit()){
		if(input->isKeyDown(PLAYER_RIGHT_KEY))            // if move right
		{
			directionX++;
		}
		if(input->isKeyDown(PLAYER_LEFT_KEY))             // if move left
		{
			directionX--;
		}
	//}



	timeInState += frameTime;
	if ((timeInState > 0.5f) && (!isDead)){ //if it has been a second or more since the last time a bullet was fired, then let the user fire another
	if(input->wasKeyPressed(VK_RETURN))
		{
			timeInState = 0; //reset the timer
			if (getShooting())
				fire();
		}
	}
	

	//=======================================================================
	//								Screenedge
	//=======================================================================
	float tempX = spriteData.x + directionX * frameTime * PlayerNS::SPEED;
	float tempY = spriteData.y + directionY * frameTime * PlayerNS::SPEED;

	if ((tempX + spriteData.width*PlayerNS::PLAYER_IMAGE_SCALE) >= GAME_WIDTH)
		directionX = 0;
	if ((tempX <= 0))
		directionX = 0;
	if ((tempY + spriteData.height*PlayerNS::PLAYER_IMAGE_SCALE) >= GAME_HEIGHT-97 || tempY <= 89)
		directionY = 0;
	spriteData.x = spriteData.x + directionX * frameTime * PlayerNS::SPEED;
	spriteData.y = spriteData.y + directionY * frameTime * PlayerNS::SPEED;

	if (!bulletsOnScreen.empty()){
		for (int i = 0; i < bulletsOnScreen.size(); i++){
			bulletsOnScreen[i].update(frameTime);
			if (bulletsOnScreen[i].getY() <= -64){ //off the top of the screen
				recycleBullet(i);
			}
		}
	}
	
	//taking damage
	if(hit)
	{
		if (!dead) {
			//timeInState += frameTime;
			//if (shaker == left && timeInState > .05){
			//	shaker = right;
			//	timeInState = 0;
			//	spriteData.x = spriteData.x + 10;
			//	update(frameTime);
			//}
			//if (shaker== right && timeInState > .05){
			//	shaker = leftleft;
			//	timeInState = 0;
			//	spriteData.x = spriteData.x - 10;
			//	update(frameTime);
			//}
			//if (shaker== leftleft && timeInState >.05){
			//	shaker = rightright;
			//	timeInState = 0;
			//	spriteData.x = spriteData.x + 10;
			//	update(frameTime);
			//}	
			//if (shaker== rightright && timeInState >.05){
			//	timeInState = 0;
			//	spriteData.x = spriteData.x - 10;
			//	update(frameTime);
			//	shaker = left;
			//}
		}
		shaker = left;
		playerHit.update(frameTime);
		if(playerHit.getAnimationComplete())
		{
			hit = false;
			setHit(false);
			playerHit.setAnimationComplete(false);
			playerHit.setCurrentFrame(PlayerNS::PLAYER_HIT_START);
			//shaker = left;
		}
	}

	if(dead)
	{
		playerDestroyed.update(frameTime);
		if(playerDestroyed.getAnimationComplete())
		{
			dead = false;
			playerDestroyed.setAnimationComplete(true);
			playerDestroyed.setCurrentFrame(PlayerNS::PLAYER_DESTROYED_START);
			
			playerDead  = true;
		}
	}
}

void PlayerShip::fire(void){
	//Bullet temp; 
	//temp = bulletVec.pop_back();
	if (!bulletVec.empty()){
		bulletsOnScreen.push_back(bulletVec.back());

		//makes the bullet be visible and detect collisions
		bulletsOnScreen.back().setVisible(true); 
		bulletsOnScreen.back().setActive(true);

		bulletsOnScreen.back().setX(spriteData.x+12.5); //sets the bullets x coordinate to whereever the PlayerShip was when it fired
		bulletsOnScreen.back().setY(spriteData.y - 50);
		bulletVec.pop_back();

		audio->playCue(PLAYERFIRE);
	}
}


void PlayerShip::recycleBullet(int index){
	bulletVec.push_back(bulletsOnScreen[index]);
	bulletsOnScreen.erase(bulletsOnScreen.begin() + index); 
}


void PlayerShip::reset(){
	score = 0;
	this->hit = false;
	this->dead = false;
	this->setVisible(true); 
	this->setActive(true);
	this->setX(GAME_WIDTH/2-50);                    // start above and left of planet
	this->setY(GAME_HEIGHT - (PlayerNS::HEIGHT + 20));
	setShooting(false);
	lives = 3;
	level2 = false;
	isDead = false;
	numberOfEnemiesKilled = 0;
}