#ifndef _PLAYER_BULLET_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _PLAYER_BULLET_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "Bulletparticle.h"
#include "constants.h"
#include "textureManager.h"

class PlayerBulletParticleManager
{
	private:
	BulletParticle particles[MAX_NUMBER_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float PlayerBulletParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	PlayerBulletParticleManager();
	~PlayerBulletParticleManager();
	void PlayerBulletParticleManager::setInvisibleAllParticles();
	void PlayerBulletParticleManager::setVisibleNParticles(int n);
	void PlayerBulletParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void PlayerBulletParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool PlayerBulletParticleManager::initialize(Graphics *g);

	void PlayerBulletParticleManager::update(float frametime);
	void PlayerBulletParticleManager::draw();


};







#endif _PLAYER_BULLET_PARTICLE_MANAGER_H