// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "SchafhauserAsteroidManager.h"

//=============================================================================
// default constructor
//=============================================================================
AsteroidManager::AsteroidManager() : Entity()
{
    
	timeInState = 0.0;
	srand (time(NULL));
	numberOfAsteroidsUsed = 0;
	totalAsteroids = 0;
	endOfLevel2 = false;

	//asteroidDead = false;
	
}

//=============================================================================
// Initialize the .
// Post: returns true if successful, false if failed
//=============================================================================
bool AsteroidManager::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
	//TODO: use this for damage
 //   asteroidDestroyed.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
 //   asteroidDestroyed.setFrames(AsteroidNS::ENEMY_DESTROYED_START, AsteroidNS::ENEMY_DESTROYED_END);
 //   asteroidDestroyed.setCurrentFrame(AsteroidNS::ENEMY_DESTROYED_START);
 //   asteroidDestroyed.setFrameDelay(AsteroidNS::ENEMY_ANIMATION_DELAY);
	//asteroidDestroyed.setLoop(false);

    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the 
//=============================================================================
void AsteroidManager::draw()
{
    Image::draw();              // draw 
	//TODO: use this for damage
	if (!AsteroidsOnScreen.empty()){
		for (int i = 0; i < AsteroidsOnScreen.size(); i++){
			AsteroidsOnScreen[i].draw();
		}
	}
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void AsteroidManager::update(float frameTime)
{
	Entity::update(frameTime);
	
	timeInState += frameTime;
	if ((timeInState > 0.3f) && (totalAsteroids < 50)){
		timeInState = 0; //reset the timer
		
		sendAsteroid();
	}

	if (numberOfAsteroidsUsed >= 50){ //the level is over
		endOfLevel2 = true;
	}

	
	if (!AsteroidsOnScreen.empty()){
		for (int i = 0; i < AsteroidsOnScreen.size(); i++){
			AsteroidsOnScreen[i].update(frameTime);

			if (AsteroidsOnScreen[i].getY() >= GAME_HEIGHT + 100){ //off the bot of the screen
				recycleAsteroid(i);
				//numberOfAsteroidsUsed++;
			}
		}
	}
}

void AsteroidManager::sendAsteroid(){
	if (!AsteroidVec.empty()){
		totalAsteroids++;
		AsteroidsOnScreen.push_back(AsteroidVec.back());

		AsteroidsOnScreen.back().setVisible(true); 
		AsteroidsOnScreen.back().setActive(true);

		//change velocity!	
		int randX = rand()%500 + (-150);
		AsteroidsOnScreen.back().setVelocity(VECTOR2(randX,250)); //y used to be 200
		AsteroidsOnScreen.back().setX((rand()%500) + 20); //sets the bullets x coordinate to whereever the EnemyShip was when it fired
		AsteroidsOnScreen.back().setY(-40); //sets the bullets y coordinate to whereever the EnemyShip was when it fired

		AsteroidVec.pop_back();

		numberOfAsteroidsUsed++;
		//audio->playCue(ENEMYFIRE);
	}
}

void AsteroidManager::recycleAsteroid(int index){
	if (index < AsteroidsOnScreen.size()){
	AsteroidVec.push_back(AsteroidsOnScreen[index]);
	AsteroidsOnScreen.erase(AsteroidsOnScreen.begin() + index); 
	}
}

void AsteroidManager::reset(){
	totalAsteroids = 0;
	numberOfAsteroidsUsed = 0;
	endOfLevel2 = false;
}

