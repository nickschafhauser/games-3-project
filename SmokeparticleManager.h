#ifndef _PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "Smokeparticle.h"
#include "constants.h"
#include "textureManager.h"

class SmokeParticleManager
{
	private:
	SmokeParticle particles[MAX_NUMBER_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float SmokeParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	SmokeParticleManager();
	~SmokeParticleManager();
	void SmokeParticleManager::setInvisibleAllParticles();
	void SmokeParticleManager::setVisibleNParticles(int n);
	void SmokeParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void SmokeParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool SmokeParticleManager::initialize(Graphics *g);

	void SmokeParticleManager::update(float frametime);
	void SmokeParticleManager::draw();


};







#endif _PARTICLE_MANAGER_H