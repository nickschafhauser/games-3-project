
              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN
#ifndef PATTERN_STEM_H                 // Prevent multiple definitions if this 
#define PATTERN_STEM_H   



//class PatternStep;

#include "constants.h"
#include "entity.h"
#include <cmath>
#include <d3d9.h>
#include <d3dx9.h>
#include "KenneyAIShip.h"

namespace patternStepNS
{
}

// inherits from Entity class
class PatternStep
{
private:
   // puckNS::DIRECTION direction;    
    bool active;                  
	float timeInStep; //accumulates the time step has executed-- time accumulated
	AIShip *entity;
	float timeForStep; //limit on the time for the step-- how long the step is executed (in seconds): consider extending
	PATTERN_STEP_ACTION action;

public:
    // constructor
    PatternStep();

    // inherited member functions
    void initialize(AIShip *e);

    void update(float frameTime);

	void setEntity(AIShip *e) {entity = e;}
	void setAction(PATTERN_STEP_ACTION a) {action = a;}
	void setTimeForStep(float time) {timeForStep = time;}
	bool isFinished() {return !active;}
	void setActive() {active = true;}
	void setInactive() {active = false;}

};
#endif



